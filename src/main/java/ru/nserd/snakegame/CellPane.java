package ru.nserd.snakegame;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import ru.nserd.snakegame.engine.Point;

public class CellPane extends Pane {
    public final int i;
    public final int j;

    public CellPane(int i, int j, int size, int type){
        this.i = i;
        this.j = j;

        Rectangle shape = new Rectangle(size, size, getColor(type));

        getChildren().add(shape);

        setTranslateX(j);
        setTranslateY(i);
    }

    private Color getColor(int type){
        Color color = null;

        if (type == Point.Value.BODY_POINT.ordinal())   color = Color.GREEN;
        if (type == Point.Value.TARGET_POINT.ordinal()) color = Color.RED;
        if (type == Point.Value.FILL_POINT.ordinal())   color = Color.DARKGREEN;

        return color;
    }
}