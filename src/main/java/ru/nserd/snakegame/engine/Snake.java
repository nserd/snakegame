package ru.nserd.snakegame.engine;

import java.util.ArrayList;

class Snake {
    static final int UP    = 0;
    static final int RIGHT = 1;
    static final int DOWN  = 2;
    static final int LEFT  = 3;

    private final ArrayList<Point> body;

    private boolean eatFoodCall = false;

    Snake(int iStart, int jStart) {
        body = new ArrayList<>();
        body.add(new Point(iStart, jStart, Point.Value.BODY_POINT));
    }

    Snake(int iStart, int jStart, int length, int bodyLocation) {
        this(iStart, jStart);

        for (int i = 1; i < length; i++) {
            switch (bodyLocation) {
                case DOWN:
                    body.add(new Point(iStart + i, jStart, Point.Value.BODY_POINT));
                    break;
                case UP:
                    body.add(new Point(iStart - i, jStart, Point.Value.BODY_POINT));
                    break;
                case LEFT:
                    body.add(new Point(iStart, jStart - i, Point.Value.BODY_POINT));
                    break;
                case RIGHT:
                    body.add(new Point(iStart, jStart + i, Point.Value.BODY_POINT));
                    break;
            }
        }
    }

    void move(int direction) {
        if (!(direction == UP || direction == DOWN || direction == LEFT || direction == RIGHT)) {
            throw new IllegalArgumentException("Value not defined.");
        }

        Point tail = body.get(body.size() - 1);

        for (int i = body.size() - 1; i > 0; i--) {
            Point previousPoint = body.get(i - 1);
            body.set(i, new Point(previousPoint.i, previousPoint.j, previousPoint.value));
        }

        // Если в хвосте оказалась съеденная пища, то увеличивает длину змейки
        if (tail.value == Point.Value.FILL_POINT) {
            body.add(new Point(tail.i, tail.j, Point.Value.BODY_POINT));
        }

        setupHead(direction);
    }

    private void setupHead(int direction) {
        stepOnNewPoint(body.get(0), direction);

        // Голове присваивается исходное значение после прохождения точки с пищей
        if (eatFoodCall) {
            Point head = body.get(0);
            body.set(0, new Point(head.i, head.j, Point.Value.BODY_POINT));

            eatFoodCall = false;
        }
    }

    /**
     * Функция для перемещения головы змейки в заданном направлении.
     */
    private void stepOnNewPoint(Point head, int direction) {
        switch (direction) {
            case UP:
                body.set(0, new Point(head.i - 1, head.j, head.value));
                break;
            case DOWN:
                body.set(0, new Point(head.i + 1, head.j, head.value));
                break;
            case LEFT:
                body.set(0, new Point(head.i, head.j - 1, head.value));
                break;
            case RIGHT:
                body.set(0, new Point(head.i, head.j + 1, head.value));
                break;
        }

        // Проверка на то, что змейка не пошла сама в себя
        for (int i = 1; i < body.size(); i++) {
            if (body.get(0).i == body.get(i).i && body.get(0).j == body.get(i).j) {
                throw new IllegalArgumentException("Snake move into itself.");
            }
        }
    }

    void eatFood() {
        Point head = body.get(0);
        body.set(0, new Point(head.i, head.j, Point.Value.FILL_POINT));

        eatFoodCall = true;
    }

    int getBodySize() {
        return body.size();
    }

    Point getHead() {
        return body.get(0);
    }

    Point getTail() {
        return body.get(body.size() - 1);
    }

    Point[] getBody() {
        Point[] body = new Point[getBodySize()];

        for (int i = 0; i < body.length; i++) {
            body[i] = this.body.get(i);
        }

        return body;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Snake: \n");

        for (Point point : body) {
            sb.append("[")
                    .append(point.value)
                    .append("]")
                    .append(" - x:")
                    .append(point.i)
                    .append(",")
                    .append("y:")
                    .append(point.j)
                    .append('\n');
        }

        sb.append('\n');

        return sb.toString();
    }
}