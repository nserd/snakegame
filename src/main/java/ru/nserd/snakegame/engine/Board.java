package ru.nserd.snakegame.engine;

import java.util.Arrays;

class Board {
    private final int[][] board;

    protected Board(int rows, int columns) {
        this.board = new int[rows][columns];

        for (int[] points : board) {
            Arrays.fill(points, Point.Value.EMPTY_POINT.ordinal());
        }
    }

    protected void setPoint(Point point) {
        try {
            board[point.i][point.j] = point.value.ordinal();
        } catch (ArrayIndexOutOfBoundsException e){
            e = new ArrayIndexOutOfBoundsException("Snake went beyond the board.\n" + e.getMessage());
            e.printStackTrace();
        }
    }

    protected int getNumbOfRows(){
        return board.length;
    }

    protected int getNumbOfColumns(){
        return board[0].length;
    }

    protected int[][] getBoard() {
        int[][] board = new int[getNumbOfRows()][getNumbOfColumns()];

        for(int i = 0; i < board.length; i++){
            System.arraycopy(this.board[i], 0, board[i], 0, this.board[i].length);
        }

        return board;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (int[] row : board) {
            sb.append(Arrays.toString(row));
            sb.append('\n');
        }

        return sb.toString();
    }
}