package ru.nserd.snakegame.engine;

import java.util.Arrays;

public class Point {
    protected final int i;
    protected final int j;

    protected final Value value;

    protected Point(int i, int j, Value value){
        this.i = i;
        this.j = j;

        if(Arrays.asList(Value.values()).contains(value))
            this.value = value;
        else
            throw new IllegalArgumentException("Value not defined.");
    }

    public enum Value {
        EMPTY_POINT,   // Пустая клетка
        BODY_POINT,    // Клетка, на которой находится часть тела змейки
        TARGET_POINT,  // Клетка с едой
        FILL_POINT     // Клетка со съеденой едой
    }
}