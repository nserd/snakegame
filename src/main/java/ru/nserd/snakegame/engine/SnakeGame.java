package ru.nserd.snakegame.engine;

import java.io.Serializable;
import java.util.Random;

public class SnakeGame implements Serializable {
    private final Board board;
    private final Snake snake;

    private int foodI;
    private int foodJ;

    private int eatenFoodCounter;

    private int directionHead;

    public SnakeGame(int boardSizeI, int boardSizeJ) {
        Random rand = new Random();

        this.board = new Board(boardSizeI, boardSizeJ);
        this.snake = new Snake(rand.nextInt(boardSizeI), rand.nextInt(boardSizeJ));

        board.setPoint(snake.getBody()[0]);

        placeFoodOnBoard();
        eatenFoodCounter = 0;

        directionHead = Snake.UP;
    }

    public SnakeGame(int boardSizeI, int boardSizeJ, int snakePosI, int snakePosJ) {
        this.board = new Board(boardSizeI, boardSizeJ);
        this.snake = new Snake(snakePosI, snakePosJ);

        board.setPoint(snake.getBody()[0]);

        placeFoodOnBoard();
        eatenFoodCounter = 0;

        directionHead = Snake.UP;
    }

    public SnakeGame(int boardSizeI, int boardSizeJ, int snakePosI, int snakePosJ, int snakeLength) {
        this.board = new Board(boardSizeI, boardSizeJ);
        this.snake = new Snake(snakePosI, snakePosJ, snakeLength, Snake.DOWN);

        for (int i = 0; i < snakeLength; i++) {
            board.setPoint(snake.getBody()[i]);
        }

        placeFoodOnBoard();
        eatenFoodCounter = 0;

        directionHead = Snake.UP;
    }

    public boolean moveForward() {
        return move(directionHead);
    }

    public boolean moveLeft() {
        directionHead = mod4(directionHead - 1);
        return move(directionHead);
    }

    public boolean moveRight() {
        directionHead = mod4(directionHead + 1);
        return move(directionHead);
    }

    /**
     * Внутренний метод, реализующий передвижение змейки по доске.
     *
     * @param direction направление движения
     * @return результат работы:
     * true - змейка переместилась успешно,
     * false - змейка уперлась в конец доски (игра окончена).
     */
    private boolean move(int direction) {
        Point snakeTailBuffer = snake.getTail();

        try { snake.move(direction); }
        catch (IllegalArgumentException e) { return false; }

        if (snake.getHead().i == foodI && snake.getHead().j == foodJ) {
            snake.eatFood();
            placeFoodOnBoard();

            eatenFoodCounter++;
        }

        return refreshBoard(snakeTailBuffer);
    }

    /**
     * Метод для обновления доски после движения змейки.
     *
     * @param snakeTailBuffer точка в которой находился хвост змейки до перемещения.
     * @return true - метод завершился успешно, false - змейка вышла за пределы доски.
     */
    private boolean refreshBoard(Point snakeTailBuffer) {
        for (Point point : snake.getBody()) {
            if (isPointInsideBoard(point)) {
                board.setPoint(point);
            } else {
                return false;
            }
        }

        cleanupAfterMove(snakeTailBuffer);

        return true;
    }

    /**
     * Обновляет клетку, которая освободилась после движения змейки
     */
    private void cleanupAfterMove(Point snakeTailBuffer) {
        if (snake.getBody().length > 1 && snakeTailBuffer.value != Point.Value.FILL_POINT) {
            //костыль: помогает правильно отрисовывать доску в случаях, если голова змеи идет прямо за её хвостом
            if (!(snakeTailBuffer.i == snake.getHead().i && snakeTailBuffer.j == snake.getHead().j)) {
                board.setPoint(new Point(snakeTailBuffer.i, snakeTailBuffer.j, Point.Value.EMPTY_POINT));
            }
        } else if (snakeTailBuffer.value != Point.Value.FILL_POINT) {
            board.setPoint(new Point(snakeTailBuffer.i, snakeTailBuffer.j, Point.Value.EMPTY_POINT));
        }
    }

    private void placeFoodOnBoard() {
        Random rand = new Random();

        int foodPos = rand.nextInt(this.board.getNumbOfRows() * this.board.getNumbOfColumns());
        setFoodXY(foodPos);

        int[][] board = this.board.getBoard();

        while (!(board[foodI][foodJ] == Point.Value.EMPTY_POINT.ordinal())) {
            foodPos = rand.nextInt(board.length * board[0].length);
            setFoodXY(foodPos);
        }

        this.board.setPoint(new Point(foodI, foodJ, Point.Value.TARGET_POINT));
    }

    private boolean isPointInsideBoard(Point point) {
        return point.i >= 0 && point.j >= 0 && point.i < board.getNumbOfRows() && point.j < board.getNumbOfColumns();
    }


    private void setFoodXY(int foodPos) {
        foodI = foodPos / board.getNumbOfColumns();
        foodJ = foodPos % board.getNumbOfColumns();
    }

    public int[] getHeadCoordinates(){
        return new int[] { snake.getHead().i, snake.getHead().j };
    }

    public int[] getTailCoordinates(){
        return new int[] { snake.getTail().i, snake.getTail().j };
    }

    public int[][] getBoard() {
        return board.getBoard();
    }

    public int getEatenFoodCounter() {
        return eatenFoodCounter;
    }

    private int mod4(int numb) {
        while (numb < 4) {
            numb += 4;
        }

        return numb % 4;
    }
}