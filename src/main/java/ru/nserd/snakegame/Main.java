package ru.nserd.snakegame;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import ru.nserd.snakegame.engine.Point;
import ru.nserd.snakegame.engine.SnakeGame;

public class Main extends Application {
    private static final int ONE_SECOND = 1000000000;

    private Pane gameRoot;

    private SnakeGame snakeGame;

    private int gameWidth;
    private int gameHeight;

    private int boardWidth;
    private int boardHeight;

    private int speed;

    private boolean turnLeft;
    private boolean turnRight;

    private SimpleBooleanProperty isGameOver;

    private int score;

    private void initialization() {
        gameWidth  = 440;
        gameHeight = 440;

        boardWidth  = 22;
        boardHeight = 22;

        speed = ONE_SECOND / 7;
        score = 0;
    }

    private Parent createContent() {
        Pane appRoot = new Pane();

        gameRoot = new Pane();
        turnLeft = turnRight = false;
        isGameOver = new SimpleBooleanProperty(false);

        snakeGame = new SnakeGame(boardWidth, boardHeight, 5 ,5, 5);
        refreshBoard(snakeGame.getBoard());

        gameRoot.setPrefSize(gameHeight, gameWidth);
        appRoot.getChildren().add(gameRoot);

        return appRoot;
    }

    private void update() {
        if (isGameOver.get()) return;

        if (turnLeft) {
            isGameOver.setValue(!(snakeGame.moveLeft()));
            turnLeft = false;
        } else if (turnRight) {
            isGameOver.setValue(!(snakeGame.moveRight()));
            turnRight = false;
        } else {
            isGameOver.setValue(!(snakeGame.moveForward()));
        }

        score = snakeGame.getEatenFoodCounter();
        refreshBoard(snakeGame.getBoard());
    }

    private void refreshBoard(int[][] board) {
        int cellWidth = gameWidth / boardWidth;
        int cellHeight = gameHeight / boardHeight;

        for (int n = 0, i = 0; n < gameHeight; n += cellHeight, i++) {
            for (int m = 0, j = 0; m < gameWidth; m += cellWidth, j++) {

                if (board[i][j] == Point.Value.EMPTY_POINT.ordinal()) {
                    for (Node node : gameRoot.getChildren()) {
                        CellPane cellPane = (CellPane) node;

                        if (cellPane.i == n && cellPane.j == m) {
                            gameRoot.getChildren().remove(cellPane);
                            break;
                        }
                    }
                } else {
                    if (!setCell(n, m, cellWidth, board[i][j])) {
                        gameRoot.getChildren().add(new CellPane(n, m, cellWidth, board[i][j]));
                    }
                }
            }
        }
    }

    /**
     * Метод, который пересоздает заполненную клетку на заданных координатах
     * @return если на координахах существует клетка, возвращает true. В противном случае - false.
     */
    private boolean setCell(int n, int m, int cellWidth, int value) {
        for (int k = 0; k < gameRoot.getChildren().size(); k++) {
            CellPane cellPane = (CellPane) gameRoot.getChildren().get(k);
            // если на текущей позиции есть заполненная клетка, то обновляем ее
            if (cellPane.i == n && cellPane.j == m) {
                gameRoot.getChildren().set(k, new CellPane(n, m, cellWidth, value));
                return true;
            }
        }

        return false;
    }

    private Scene initGameOverScene() {
        BorderPane borderPane = new BorderPane();

        AnchorPane anchorPaneButtons = new AnchorPane();
        AnchorPane anchorPaneLabel   = new AnchorPane();

        Scene gameOverScene = new Scene(borderPane, 230, 100);

        Label gameOverLabel = new Label("Игра окончена");
        Label scoreLabel    = new Label("Счет: " + score);

        Button newGameB = new Button("Новая игра");
        Button exitB    = new Button("Выход");

        newGameB.setOnAction(actionEvent -> {
            snakeGame =  new SnakeGame(boardWidth, boardHeight, 5 ,5, 5);
            refreshBoard(snakeGame.getBoard());

            score = 0;
            isGameOver.set(false);

            Stage stage = (Stage) newGameB.getScene().getWindow();
            stage.close(); //закрытие окна после нажатия кнопки
        });

        exitB.setOnAction(actionEvent -> System.exit(0));

        gameOverLabel.setFont(new Font("Arial", 15));
        gameOverLabel.setAlignment(Pos.CENTER);

        AnchorPane.setLeftAnchor(gameOverLabel, 10d);
        AnchorPane.setRightAnchor(gameOverLabel, 10d);

        AnchorPane.setLeftAnchor(newGameB, 10d);
        AnchorPane.setBottomAnchor(newGameB, 10d);

        AnchorPane.setRightAnchor(exitB, 10d);
        AnchorPane.setBottomAnchor(exitB, 10d);

        anchorPaneButtons.getChildren().addAll(newGameB, exitB);
        anchorPaneLabel.getChildren().add(gameOverLabel);

        borderPane.setTop(anchorPaneLabel);
        borderPane.setCenter(scoreLabel);
        borderPane.setBottom(anchorPaneButtons);

        return gameOverScene;
    }

    @Override
    public void start(Stage primaryStage) {
        initialization();

        Scene scene = new Scene(createContent());

        scene.setOnKeyPressed(keyEvent -> {
            turnLeft = keyEvent.getCode().equals(KeyCode.A) || keyEvent.getCode() == KeyCode.LEFT;
            turnRight = keyEvent.getCode().equals(KeyCode.D) || keyEvent.getCode() == KeyCode.RIGHT;
        });

        isGameOver.addListener((observableValue, oldValue, newValue) -> {
            if (newValue) {
                Stage gameOverWindow = new Stage();
                gameOverWindow.initModality(Modality.WINDOW_MODAL);
                gameOverWindow.initOwner(primaryStage);

                Scene gameOverScene = initGameOverScene();

                gameOverWindow.initStyle(StageStyle.UNDECORATED);
                gameOverWindow.setScene(gameOverScene);

                //выставление окна в центре окна с игрой
                gameOverWindow.setX((primaryStage.getX() + (double) gameWidth / 2) - gameOverScene.getWidth() / 2);
                gameOverWindow.setY((primaryStage.getY() + (double) gameHeight / 2) - gameOverScene.getHeight() / 2);

                gameOverWindow.show();
            }
        });

        primaryStage.setScene(scene);
        primaryStage.show();

        new AnimationTimer() {
            private long currentTime;

            @Override
            public void handle(long l) {
                if (l > currentTime + speed) {
                    update();
                    currentTime = l;
                }
            }
        }.start();
    }

    public static void main(String[] args) {
        launch(args);
    }
}