package ru.nserd.snakegame.engine;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

public class SnakeTest {
    Snake snake;

    @Before
    public void setup() {
        Random random = new Random();
        snake = new Snake(random.nextInt(1000), random.nextInt(1000));
    }

    @Test
    public void move() {
        Point head;

        head = snake.getHead();
        snake.move(Snake.DOWN);
        Assert.assertEquals(head.i, snake.getHead().i - 1);

        head = snake.getHead();
        snake.move(Snake.LEFT);
        Assert.assertEquals(head.j, snake.getHead().j + 1);

        head = snake.getHead();
        snake.move(Snake.UP);
        Assert.assertEquals(head.i, snake.getHead().i + 1);

        head = snake.getHead();
        snake.move(Snake.RIGHT);
        Assert.assertEquals(head.j, snake.getHead().j - 1);
    }

    @Test
    public void eatFood() {
        snake.eatFood();
        Assert.assertEquals(Point.Value.FILL_POINT, snake.getHead().value);
    }
}